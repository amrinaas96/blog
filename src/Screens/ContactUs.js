import React, { Component } from "react";
import { Button } from "reactstrap";

export default class ContactUs extends Component {
  constructor() {
    super();
    this.state = {
      name: " ",
      message: " ",
    };
  }

  handleSubmit = () =>
    alert(`Hi ${this.state.name}, thank you for your feedback!`);

  render() {
    const { name, message } = this.state;

    return (
      <form className="form-container" onSubmit={this.handleSubmit}>
        <div className="form-wraper">
          <div>Input Your Name</div>
          <input
            type="text"
            value={name}
            placeholder="Full Name"
            onChange={(event) => this.setState({ name: event.target.value })}
          />
        </div>
        <div className="form-wraper">
          <div>Your Message</div>
          <textarea
            type="textarea"
            value={message}
            placeholder="Write your message here!"
            onChange={(event) => this.setState({ message: event.target.value })}
          />
        </div>
        <Button color="success">Send!</Button>{" "}
      </form>
    );
  }
}
