import React from "react";

export default function Post({ title, content, img }) {
  return (
    <div>
      <h3>{title}</h3>
      <img src={img} alt="machu-pichu" />
      <p>{content}</p>
    </div>
  );
}
