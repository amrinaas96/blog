import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Home from "./Screens/Home";
import Profile from "./Screens/Profile";
import ContactUs from "./Screens/ContactUs";
import Navigation from "./Screens/Navigation";
import Blog from "./Screens/Blog";

function App() {
  return (
    <Router>
      <Navigation />

      <Switch>
        <Route component={Home} path="/" exact={true} />
        <Route component={Profile} path="/profile" />
        <Route component={ContactUs} path="/contact-us" />
        <Route component={Blog} path="/blog" />
      </Switch>
    </Router>
  );
}

export default App;
